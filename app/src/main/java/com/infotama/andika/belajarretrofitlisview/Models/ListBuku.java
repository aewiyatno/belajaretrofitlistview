package com.infotama.andika.belajarretrofitlisview.Models;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Created by andika on 9/8/16.
 */

public class ListBuku {
    private Integer idbuku;
    private String nama;
    private Integer harga;
    private String status;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Integer getIdbuku() {
        return idbuku;
    }

    public void setIdbuku(Integer idbuku) {
        this.idbuku = idbuku;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public Integer getHarga() {
        return harga;
    }

    public void setHarga(Integer harga) {
        this.harga = harga;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperties(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
}
