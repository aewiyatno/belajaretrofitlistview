package com.infotama.andika.belajarretrofitlisview.Service;

import com.infotama.andika.belajarretrofitlisview.Models.Model;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by andika on 9/8/16.
 */
public interface RestApi {
    @GET("example/buku/")
    Call<Model> loadListBuku();
}
