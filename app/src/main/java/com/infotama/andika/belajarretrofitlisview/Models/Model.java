package com.infotama.andika.belajarretrofitlisview.Models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by andika on 9/8/16.
 */
public class Model {
    private List<ListBuku>listbuku = new ArrayList<ListBuku>();

    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public List<ListBuku> getListbuku() {
        return listbuku;
    }

    public void setListbuku(List<ListBuku> listbuku) {
        this.listbuku = listbuku;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperties(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
}
