package com.infotama.andika.belajarretrofitlisview;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.infotama.andika.belajarretrofitlisview.Models.ListBuku;
import com.infotama.andika.belajarretrofitlisview.Models.Model;
import com.infotama.andika.belajarretrofitlisview.Service.RestApi;

import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    public static final String ROOT_URL = "http://api.teknorial.com";
    public static final String ID_BUKU = "id_buku";
    public static final String NAMA_BUKU = "nama_buku";
    public static final String HARGA_BUKU = "harga_buku";
    public static final String STATUS_BUKU = "status_buku";

    private ListView listView;
    private List<ListBuku> books;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = (ListView)findViewById(R.id.listViewBuku);

        getBuku();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(MainActivity.this,DetailBukuActivity.class);
                ListBuku listBuku = books.get(position);
                intent.putExtra(NAMA_BUKU,listBuku.getNama());
                intent.putExtra(HARGA_BUKU,listBuku.getHarga());
                intent.putExtra(STATUS_BUKU,listBuku.getStatus());
                intent.putExtra(ID_BUKU,listBuku.getIdbuku());

                startActivity(intent);
            }
        });
    }

    private void getBuku() {
       final ProgressDialog loading = ProgressDialog.show(this,"Fetching Data","Please wait...", false, false);

//        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
//        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
//
//        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
//        httpClient.addInterceptor(logging);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ROOT_URL)
                .addConverterFactory(GsonConverterFactory.create())
//                .client(httpClient.build())
                .build();

        RestApi service = retrofit.create(RestApi.class);

        Call<Model> call = service.loadListBuku();
        call.enqueue(new Callback<Model>() {
            @Override
            public void onResponse(Call<Model> call, Response<Model> response) {
                loading.dismiss();

                List<ListBuku> buku = response.body().getListbuku();

                books = buku;
                showList();
            }

            @Override
            public void onFailure(Call<Model> call, Throwable t) {

            }
        });
    }

    private void showList(){
        String[] items = new String[books.size()];

        for (int increment = 0; increment < books.size();increment++) {
            items[increment] = books.get(increment).getNama();
        }

        ArrayAdapter adapter = new ArrayAdapter<String>(this,R.layout.activity_listview,R.id.textView,items);

        listView.setAdapter(adapter);

    }

//    @Override
//    public void onItemClick(AdapterView<?> parent, View view, int position, long id){
//        Toast.makeText(MainActivity.this, "Clicked "+ position,Toast.LENGTH_SHORT).show();

//        Intent intent = new Intent(this, DetailBukuActivity.class);
//
//        ListBuku listBuku = books.get(position);
//        intent.putExtra(NAMA_BUKU,listBuku.getNama());
//        intent.putExtra(HARGA_BUKU,listBuku.getHarga());
//        intent.putExtra(STATUS_BUKU,listBuku.getStatus());
//        intent.putExtra(ID_BUKU,listBuku.getIdbuku());
//
//        startActivity(intent);
//    }
}
