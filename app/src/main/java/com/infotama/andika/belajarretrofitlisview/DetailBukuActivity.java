package com.infotama.andika.belajarretrofitlisview;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class DetailBukuActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_buku);

        TextView textId = (TextView) findViewById(R.id.text_id);
        TextView textNama = (TextView) findViewById(R.id.text_nama);
        TextView textHarga = (TextView)findViewById(R.id.text_harga);
        TextView textStatus = (TextView) findViewById(R.id.text_status);

        Intent intent = getIntent();

        textId.setText("ID: "+ String.valueOf(intent.getIntExtra(MainActivity.ID_BUKU,0)));
        textNama.setText("Nama: "+ intent.getStringExtra(MainActivity.NAMA_BUKU));
        textHarga.setText("Harga: "+ String.valueOf(intent.getIntExtra(MainActivity.HARGA_BUKU,0)));
        textStatus.setText("Status "+ intent.getStringExtra(MainActivity.STATUS_BUKU));

    }
}
